package br.com.mvfsilva.evaluationmobile;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by marcus.silva on 30/03/2016.
 */
public class RegisterAdapter extends BaseAdapter {
    Context ctx;
    List<RegisterObj> registers;

    public RegisterAdapter(Context ctx, List<RegisterObj> registers){
        this.ctx = ctx;
        this.registers = registers;
    }

    @Override
    public int getCount() {
        return registers.size();
    }
    @Override
    public Object getItem(int position) {
        return registers.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder{
        TextView txtCpf;
        TextView txtName;
        TextView txtAndress;
        TextView txtPhone;
        TextView txtGender;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        RegisterObj register = registers.get(position);
        ViewHolder holder = null;

        if(convertView == null){
            convertView = LayoutInflater.from(ctx).inflate(R.layout.activity_item_register, null);
            holder = new ViewHolder();
            holder.txtCpf = (TextView) convertView.findViewById(R.id.txtCpf);
            holder.txtName = (TextView) convertView.findViewById(R.id.txtName);
            holder.txtAndress = (TextView) convertView.findViewById(R.id.txtAndress);
            holder.txtPhone = (TextView) convertView.findViewById(R.id.txtPhone);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        holder.txtCpf.setText(register.getCpf());
        holder.txtName.setText(register.getName());
        holder.txtAndress.setText(register.getAndress());
        holder.txtPhone.setText(register.getPhone());
        switch(register.getGender()){
            case 0 : holder.txtGender.setText("Masculino");break;
            case 1: holder.txtGender.setText("Feminino");break;
        }
        return convertView;
    }
}
