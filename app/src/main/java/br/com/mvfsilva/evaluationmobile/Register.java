package br.com.mvfsilva.evaluationmobile;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;


public class Register extends AppCompatActivity {
    private static char FEMALE = 'F';
    private static char MALE = 'M';
    boolean status;
    char rdSexy;
    private EditText cpf, name, andress, phone;
    private RadioGroup radioSex;
    private Switch mySwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Recovery Views
        cpf     = (EditText) findViewById(R.id.cpf);
        name    = (EditText) findViewById(R.id.name);
        andress = (EditText) findViewById(R.id.andress);
        phone   = (EditText) findViewById(R.id.phone);
        radioSex = (RadioGroup) findViewById(R.id.radioSex);
        mySwitch = (Switch) findViewById(R.id.mySwitch);
        mySwitch.setChecked(true);

        cpf.addTextChangedListener(new TextWatcher() {
            boolean isUpdating;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int after) {
                if (isUpdating) {
                    isUpdating = false;
                    return;
                }

                boolean hasMask = s.toString().indexOf('.') > -1 || s.toString().indexOf('-') > -1;
                String str = s.toString().replaceAll("[.]", "").replaceAll("[-]", "");

                if (after > before) {
                    if (str.length() > 9) {
                        str = str.substring(0, 3) + '.' + str.substring(3, 6) + '.' + str.substring(6, 9) + '-' + str.substring(9, str.length());
                    } else if (str.length() > 6) {
                        str = str.substring(0, 3) + '.' + str.substring(3, 6) + '.' + str.substring(6, str.length());
                    } else if (str.length() > 3) {
                        str = str.substring(0, 3) + '.' + str.substring(3, str.length());
                    }

                    isUpdating = true;
                    cpf.setText(str);
                    cpf.setSelection(cpf.getText().length());

                } else {
                    isUpdating = true;
                    cpf.setText(str);
                    cpf.setSelection(Math.max(0, Math.min(hasMask ? start - before : start, str.length())));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public void saveForm(View view){
        String txtCpf       = cpf.getText().toString();
        String txtName      = name.getText().toString();
        String txtAndress   = andress.getText().toString();
        String txtPhone     = phone.getText().toString();

        mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) status = true;
                else status = false;
            }
        });

        int gender = radioSex.getCheckedRadioButtonId();

        if(gender == R.id.radioFemale) rdSexy = FEMALE;
        else rdSexy = MALE;

        Intent intent = new Intent(this, RegisterList.class);

        intent.putExtra("CPF", txtCpf);
        intent.putExtra("name", txtName);
        intent.putExtra("andress", txtAndress);
        intent.putExtra("phone", txtPhone);
        intent.putExtra("status", status);
        intent.putExtra("gender", rdSexy);

        startActivity(intent);
    }
}
