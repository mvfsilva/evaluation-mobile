package br.com.mvfsilva.evaluationmobile;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by marcus.silva on 30/03/2016.
 */
public class DataBaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "evalution.db";
    public static final String TABLE_NAME ="evalution_table";
    public static final int VERSION = 1;

    //(cpf-String, nome-String, endereço-String , telefone-Sting, sexo - Char, ativo - boolean)
    public DataBaseHelper(Context context){
        super(context, DATABASE_NAME, null, VERSION);
        SQLiteDatabase db = this.getWritableDatabase();

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME
                        + "( ID INTEGER PRIMARY KEY   AUTOINCREMENT, "
                        + "CPF TEXT, "
                        + "NAME TEXT, "
                        + "ANDRESS TEXT, "
                        + "PHONE TEXT, "
                        + "GENDER CHARACTER(10), "
                        + "STATUS BOOLEAN);"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVesion) {
        db.execSQL("DROP TABLE IF EXISTS" + TABLE_NAME);
        onCreate(db);
    }
}
