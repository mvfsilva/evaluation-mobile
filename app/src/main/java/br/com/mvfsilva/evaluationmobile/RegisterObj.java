package br.com.mvfsilva.evaluationmobile;

/**
 * Created by marcus.silva on 30/03/2016.
 */
public class RegisterObj {
    private String cpf;
    private String name;
    private String andress;
    private String phone;
    private char gender;
    private boolean status;

    public RegisterObj(String cpf, String name, String andress, String phone, char gender, boolean status) {
        this.cpf = cpf;
        this.name = name;
        this.andress = andress;
        this.phone = phone;
        this.gender = gender;
        this.status = status;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAndress() {
        return andress;
    }

    public void setAndress(String andress) {
        this.andress = andress;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
