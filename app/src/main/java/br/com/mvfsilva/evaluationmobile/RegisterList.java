package br.com.mvfsilva.evaluationmobile;

import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marcus.silva on 29/03/2016.
 */
public class RegisterList extends ListActivity {
    ListView listView;
    List<RegisterObj> registerObjs;
    RegisterAdapter adapter;
    String name, andress, phone, cpf;
    boolean status;
    char gender;

    Uri uri = null;
    Intent intent;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        Intent it = getIntent();

        name    = it.getStringExtra("name");
        cpf     = it.getStringExtra("cpf");
        phone   = it.getStringExtra("phone");
        andress = it.getStringExtra("andress");
        status  = it.getBooleanExtra("status", true);
        gender  = it.getCharExtra("rdSexy", ' ');

        listView = (ListView) findViewById(android.R.id.list);

        registerObjs = new ArrayList<RegisterObj>();
        registerObjs.add(new RegisterObj(cpf, name, andress, phone, gender, status));
        registerObjs.add(new RegisterObj("000.000.000-00", "Maria Joaquina", "Cabula V", "99999-9999", 'F', true));
        registerObjs.add(new RegisterObj("111.111.111-11", "Joao Paquita", "Pituba", "88888-8888", 'M', true));
        registerObjs.add(new RegisterObj("222.222.222-22", "Batman", "Canela", "77777-7777", 'M', true));

        adapter = new RegisterAdapter(this, registerObjs);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                RegisterObj registerObj = (RegisterObj) adapterView.getItemAtPosition(position);
                uri = Uri.parse("tel:" + registerObj.getPhone());
                intent = new Intent(Intent.ACTION_DIAL, uri);
                startActivity(intent);
            }
        });

        listView.setAdapter(adapter);

        // criando cabeçalho
        final int PADDING = 20;
        TextView txtHeader = new TextView(this);
        txtHeader.setBackgroundColor(Color.parseColor("#6AED83"));
        txtHeader.setTextColor(Color.WHITE);
        txtHeader.setText(R.string.app_name);
        txtHeader.setPadding(PADDING, PADDING, 0, PADDING);
        listView.addHeaderView(txtHeader);

        // criando rodapé
        TextView txtFooter = new TextView(this);
        txtFooter.setText(getResources().getQuantityString(
                R.plurals.texto_rodape,
                adapter.getCount(),
                adapter.getCount()));
        txtFooter.setBackgroundColor(Color.parseColor("#6AED83"));
        txtFooter.setGravity(Gravity.CENTER);
        txtFooter.setPadding(0, PADDING, PADDING, PADDING);
        listView.addFooterView(txtFooter);

    }
}
